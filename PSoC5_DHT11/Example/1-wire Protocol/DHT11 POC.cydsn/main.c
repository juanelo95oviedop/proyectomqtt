/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "math.h"

static int temperature=99;
static int humidity=99;
static uint8 IState,i;
float rs=0;
float ro=0;
float ppm=0;
int DHTread()
{    
    uint8 bits[5];
    uint8 cnt = 7;
    uint8 idx = 0;
    int   calc=0;
    int   timeout=0;
    for (i=0; i< 5; i++) 
        bits[i] = 0;
    DHT_Write(0u);
    CyDelay(19);
    DHT_Write(1u);
    while(DHT_Read()==1)
    {
        timeout++;
        if(timeout>500)
            return 99;  //Write overflow return function
    }
    while(DHT_Read()==0)
    {        
        timeout++;
        if(timeout>500)
            return 99;  //Write overflow return function
    }
    calc=timeout;
    timeout=0;
    while(DHT_Read()==1);
    for (i=0; i<40; i++)
    {
        timeout=0;
        while(DHT_Read()==0);
        while(DHT_Read()==1)
            timeout++;
        //Data acquiring area point
        if ((timeout) > (calc/2))
        //  UART_UartPutChar(49);
            bits[idx] |= (1 << cnt);

        if (cnt == 0)   // next byte?
        {
            cnt = 7;    // restart at MSB
            idx++;      // next byte!
        }
        else cnt--;
    }
    humidity    = bits[0]; 
    temperature = bits[2]; 
//    char symbol[60];
//    sprintf(symbol,"Temperature:%c\tHumidity%c\r\n",temperature,humidity);
//    UART_PutString(symbol);
   
    return 0;
}
void ppmMeasure(){
    uint16 adcresult=0;
    float voltaje=0;
    ro=76.63;
    adcresult=ADC_GetResult16();
    voltaje=adcresult*5/1023;
    ppm=(((1024*20000/voltaje)-20000)/ro)/5.5973021420;
    float ppm1=powf(ppm,-0.365425824);
  
    
}
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    char outputstring[40];
    UART_Start();
    ADC_Start();
    ADC_StartConvert();
    
    for(;;)
    {
        ppmMeasure();
        DHTread();
        sprintf(outputstring,"T=%i, H=%i, ppm=%i \n",temperature,humidity,(int)ppm);
        
        UART_PutString(outputstring);
        CyDelay(1000);  //Delay in milli seconds.        
    }
}

/* [] END OF FILE */
