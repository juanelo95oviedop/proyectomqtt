/*******************************************************************************
* File Name: MQ135.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_MQ135_H) /* Pins MQ135_H */
#define CY_PINS_MQ135_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "MQ135_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 MQ135__PORT == 15 && ((MQ135__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    MQ135_Write(uint8 value);
void    MQ135_SetDriveMode(uint8 mode);
uint8   MQ135_ReadDataReg(void);
uint8   MQ135_Read(void);
void    MQ135_SetInterruptMode(uint16 position, uint16 mode);
uint8   MQ135_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the MQ135_SetDriveMode() function.
     *  @{
     */
        #define MQ135_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define MQ135_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define MQ135_DM_RES_UP          PIN_DM_RES_UP
        #define MQ135_DM_RES_DWN         PIN_DM_RES_DWN
        #define MQ135_DM_OD_LO           PIN_DM_OD_LO
        #define MQ135_DM_OD_HI           PIN_DM_OD_HI
        #define MQ135_DM_STRONG          PIN_DM_STRONG
        #define MQ135_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define MQ135_MASK               MQ135__MASK
#define MQ135_SHIFT              MQ135__SHIFT
#define MQ135_WIDTH              1u

/* Interrupt constants */
#if defined(MQ135__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in MQ135_SetInterruptMode() function.
     *  @{
     */
        #define MQ135_INTR_NONE      (uint16)(0x0000u)
        #define MQ135_INTR_RISING    (uint16)(0x0001u)
        #define MQ135_INTR_FALLING   (uint16)(0x0002u)
        #define MQ135_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define MQ135_INTR_MASK      (0x01u) 
#endif /* (MQ135__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define MQ135_PS                     (* (reg8 *) MQ135__PS)
/* Data Register */
#define MQ135_DR                     (* (reg8 *) MQ135__DR)
/* Port Number */
#define MQ135_PRT_NUM                (* (reg8 *) MQ135__PRT) 
/* Connect to Analog Globals */                                                  
#define MQ135_AG                     (* (reg8 *) MQ135__AG)                       
/* Analog MUX bux enable */
#define MQ135_AMUX                   (* (reg8 *) MQ135__AMUX) 
/* Bidirectional Enable */                                                        
#define MQ135_BIE                    (* (reg8 *) MQ135__BIE)
/* Bit-mask for Aliased Register Access */
#define MQ135_BIT_MASK               (* (reg8 *) MQ135__BIT_MASK)
/* Bypass Enable */
#define MQ135_BYP                    (* (reg8 *) MQ135__BYP)
/* Port wide control signals */                                                   
#define MQ135_CTL                    (* (reg8 *) MQ135__CTL)
/* Drive Modes */
#define MQ135_DM0                    (* (reg8 *) MQ135__DM0) 
#define MQ135_DM1                    (* (reg8 *) MQ135__DM1)
#define MQ135_DM2                    (* (reg8 *) MQ135__DM2) 
/* Input Buffer Disable Override */
#define MQ135_INP_DIS                (* (reg8 *) MQ135__INP_DIS)
/* LCD Common or Segment Drive */
#define MQ135_LCD_COM_SEG            (* (reg8 *) MQ135__LCD_COM_SEG)
/* Enable Segment LCD */
#define MQ135_LCD_EN                 (* (reg8 *) MQ135__LCD_EN)
/* Slew Rate Control */
#define MQ135_SLW                    (* (reg8 *) MQ135__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define MQ135_PRTDSI__CAPS_SEL       (* (reg8 *) MQ135__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define MQ135_PRTDSI__DBL_SYNC_IN    (* (reg8 *) MQ135__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define MQ135_PRTDSI__OE_SEL0        (* (reg8 *) MQ135__PRTDSI__OE_SEL0) 
#define MQ135_PRTDSI__OE_SEL1        (* (reg8 *) MQ135__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define MQ135_PRTDSI__OUT_SEL0       (* (reg8 *) MQ135__PRTDSI__OUT_SEL0) 
#define MQ135_PRTDSI__OUT_SEL1       (* (reg8 *) MQ135__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define MQ135_PRTDSI__SYNC_OUT       (* (reg8 *) MQ135__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(MQ135__SIO_CFG)
    #define MQ135_SIO_HYST_EN        (* (reg8 *) MQ135__SIO_HYST_EN)
    #define MQ135_SIO_REG_HIFREQ     (* (reg8 *) MQ135__SIO_REG_HIFREQ)
    #define MQ135_SIO_CFG            (* (reg8 *) MQ135__SIO_CFG)
    #define MQ135_SIO_DIFF           (* (reg8 *) MQ135__SIO_DIFF)
#endif /* (MQ135__SIO_CFG) */

/* Interrupt Registers */
#if defined(MQ135__INTSTAT)
    #define MQ135_INTSTAT            (* (reg8 *) MQ135__INTSTAT)
    #define MQ135_SNAP               (* (reg8 *) MQ135__SNAP)
    
	#define MQ135_0_INTTYPE_REG 		(* (reg8 *) MQ135__0__INTTYPE)
#endif /* (MQ135__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_MQ135_H */


/* [] END OF FILE */
