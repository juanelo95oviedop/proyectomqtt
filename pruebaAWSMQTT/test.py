import time
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import asyncio
import nest_asyncio
nest_asyncio.apply()
import serial
import random
import json
from datetime import date
from datetime import datetime
PuertoSerie = serial.Serial('COM3', 115200)

def Humedad(self,params,packet):
    print("Mensaje recibido de AWS IoT Core")
    print("Topico: "+ packet.topic)
    print("Mensaje: ", (packet.payload))
    
MyMQTTClient = AWSIoTMQTTClient("Humedad")

#MyMQTTClient.configureEndpoint("am74972wsv881-ats.iot.us-east-2.amazonaws.com", 8883) #endpoint mio
MyMQTTClient.configureEndpoint("a3tq9u60wxwz92-ats.iot.us-east-1.amazonaws.com", 8883) #endpoint oscar
# se definen los objetos para el acceso, debe estar la ruta local de los archivos creados al crear el objeto en aws
rootCA="C:/Users/juano/Documents/JuanPabloOviedoPerdomo/universidad/posgrados/MCIC/segundoSemestre/AplicacionesWeb/proyectoFinal/pruebaAWSMQTT/temperatura1/root-CA.crt"
privateKey="C:/Users/juano/Documents/JuanPabloOviedoPerdomo/universidad/posgrados/MCIC/segundoSemestre/AplicacionesWeb/proyectoFinal/pruebaAWSMQTT/temperatura1/temperatura2.private.key"
certificate="C:/Users/juano/Documents/JuanPabloOviedoPerdomo/universidad/posgrados/MCIC/segundoSemestre/AplicacionesWeb/proyectoFinal/pruebaAWSMQTT/temperatura1/temperatura2.cert.pem"


MyMQTTClient.configureCredentials(rootCA,privateKey,certificate)
MyMQTTClient.configureOfflinePublishQueueing(-1)
MyMQTTClient.configureDrainingFrequency(2)
MyMQTTClient.configureConnectDisconnectTimeout(10)
MyMQTTClient.configureMQTTOperationTimeout(5)


print("Iniciando IoT Core topico")

async def run(i1,i2,i3,idmessage1,idmessage2,idmessage3):
    global payload1, payload2, payload3
    # leemos hasta que encontarmos el final de linea
    serie = PuertoSerie.readline()
    # Mostramos el valor leido y eliminamos el salto de linea del final
    #print( "Valor PSoC: " , serie)
    datosCaracter=""
    Temp  = (serie[2:4])
    for valor in Temp:
        datosCaracter = datosCaracter + chr(valor)
    Temp = datosCaracter
    Humed = (serie[8:10])
    datosCaracter=""
    for valor in Humed:
        datosCaracter = datosCaracter + chr(valor)
    Humed = datosCaracter
    ppm = (serie[16:19])
    datosCaracter=""
    for valor in ppm:
        datosCaracter = datosCaracter + chr(valor)
    ppm = datosCaracter
    now=datetime.now()
    
    payload1={"sensorId": 1, "datetime": int(now.strftime("%Y%m%d%H%M%S")), "Mode": "read", "Idmessage": idmessage1, "sensor_read__response": {"sensorName": "humedad2", "topic": "gateway1/response/read/humedad1", "Value": 100, "measurement": "relativa"}}
    payload2={"sensorId": 2, "datetime": int(now.strftime("%Y%m%d%H%M%S")), "Mode": "read", "Idmessage": idmessage2, "sensor_read__response": {"sensorName": "temperatura2", "topic": "gateway1/response/read/temperatura1", "Value": 100, "measurement": "relativa"}}
    payload3={"sensorId": 3, "datetime": int(now.strftime("%Y%m%d%H%M%S")), "Mode": "read", "Idmessage": idmessage3, "sensor_read__response": {"sensorName": "PPM2", "topic": "gateway1/response/read/co2", "Value": 100, "measurement": "relativa"}}
    print(payload1)
    print(payload2)
    print(payload3)
    time.sleep(25)
    #MyMQTTClient.subscribe("Humedad", 0, Humedad)
async def run2():
    MyMQTTClient.publishAsync(
        topic="gateway1/response/read/humedad2",
        QoS=1,
        payload=json.dumps(payload1)
    )
    MyMQTTClient.publishAsync(
        topic="gateway1/response/read/temperatura2",
        QoS=1,
        payload=json.dumps(payload2)
    )
    
    MyMQTTClient.publishAsync(
        topic="gateway1/response/read/co2",
        QoS=1,
        payload=json.dumps(payload3)
    )
    
    
#ciclo infinito que envía y lee del mismo topico la información. 

def configTemperatura(self,params,packet):
    print("Mensaje recibido de AWS IoT Core")
    print("Topico " + packet.topic)
    print("Mensaje " , (packet.payload))
#MyMQTTClient.subscribe("$aws/things/Sensor1/configTemperatura", 0, configTemperatura)
idmessage1=0
idmessage2=0
idmessage3=0
MyMQTTClient.connect()
while (True):
    i1=random.randint(0, 20) 
    i2=random.randint(0, 20) 
    i3=random.randint(0, 20)
    idmessage1=idmessage1+1
    idmessage2=idmessage2+1
    idmessage3=idmessage3+1
    try:
        # medición de datos
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run(i1,i2,i3,idmessage1,idmessage2,idmessage3))
        #publicación en AWS
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run2())
        
    except:
        MyMQTTClient.connect()
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run(i1,i2,i3,idmessage1,idmessage2,idmessage3))
        #publicación en AWS
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run2())
    