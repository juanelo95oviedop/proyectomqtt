import time
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import asyncio
import nest_asyncio
nest_asyncio.apply()
import serial
import random
PuertoSerie = serial.Serial('COM4', 115200)
ppm_flag=0
humed_flag=0
temp_flag=0
def Humedad(self,params,packet):
    global humed_flag
    print("Mensaje recibido de AWS IoT Core")
    print("Topico " + packet.topic)
    print("Mensaje " , (packet.payload[-4:-1]))
    #"{\"sensorId\" :3, \"mode\":activate, \"sensorName\":actuador1, \"state\": OFF}"
    option=packet.payload[-4:-1]
    option=option.decode()
    print(option)
    if(option=='OFF'):
        humed_flag=0
    elif(option==' ON'):
        humed_flag=1
    print(humed_flag)
def CO2(self,params,packet):
    global ppm_flag
    print("Mensaje recibido de AWS IoT Core")
    print("Topico " + packet.topic)
    print("Mensaje " , (packet.payload[-4:-1]))
    option=packet.payload[-4:-1]
    option=option.decode()
    if(option=='OFF'):
        ppm_flag=0
    elif(option==' ON'):
        ppm_flag=1
MyMQTTClient = AWSIoTMQTTClient("actuador2")

#MyMQTTClient.configureEndpoint("am74972wsv881-ats.iot.us-east-2.amazonaws.com", 8883) #endpoint mio
MyMQTTClient.configureEndpoint("a3tq9u60wxwz92-ats.iot.us-east-1.amazonaws.com", 8883) #endpoint oscar
# se definen los objetos para el acceso, debe estar la ruta local de los archivos creados al crear el objeto en aws
rootCA="C:/Users/juano/Documents/JuanPabloOviedoPerdomo/universidad/posgrados/MCIC/segundoSemestre/AplicacionesWeb/proyectoFinal/pruebaAWSMQTT/accesoactuadores/root-CA.crt"
privateKey="C:/Users/juano/Documents/JuanPabloOviedoPerdomo/universidad/posgrados/MCIC/segundoSemestre/AplicacionesWeb/proyectoFinal/pruebaAWSMQTT/accesoactuadores/actuador2.private.key"
certificate="C:/Users/juano/Documents/JuanPabloOviedoPerdomo/universidad/posgrados/MCIC/segundoSemestre/AplicacionesWeb/proyectoFinal/pruebaAWSMQTT/accesoactuadores/actuador2.cert.pem"

MyMQTTClient.configureCredentials(rootCA,privateKey,certificate)

MyMQTTClient.configureOfflinePublishQueueing(-1)
MyMQTTClient.configureDrainingFrequency(2)
MyMQTTClient.configureConnectDisconnectTimeout(10)
MyMQTTClient.configureMQTTOperationTimeout(5)

MyMQTTClient.connect()
MyMQTTClient.subscribe("gateway1/response/read/actuador2", 0, Humedad)
MyMQTTClient.subscribe("gateway1/response/read/actuador3", 0, CO2)


print("Iniciando IoT Core topico")
while (True):
    
    temp_flag=random.randint(0,1)-0.35
    if(temp_flag<=0.6):
        temp_flag=0
    else:
        temp_flag=1
    print(temp_flag)
    if(temp_flag==0 and humed_flag==0 and ppm_flag==0):
        PuertoSerie.write(b'1')
    elif(temp_flag==1 and humed_flag==0 and ppm_flag==0):
        PuertoSerie.write(b'2')
    elif(temp_flag==0 and humed_flag==1 and ppm_flag==0):
        PuertoSerie.write(b'3')
    elif(temp_flag==0 and humed_flag==0 and ppm_flag==1):
        PuertoSerie.write(b'4')
    elif(temp_flag==1 and humed_flag==1 and ppm_flag==0):
        PuertoSerie.write(b'5')
    elif(temp_flag==0 and humed_flag==1 and ppm_flag==1):
        PuertoSerie.write(b'6')
    elif(temp_flag==1 and humed_flag==0 and ppm_flag==1):
        PuertoSerie.write(b'7')
    elif(temp_flag==1 and humed_flag==1 and ppm_flag==1):
        PuertoSerie.write(b'8')
    print(temp_flag,humed_flag,ppm_flag)
    time.sleep(5)