var awsIot = require('aws-iot-device-sdk');
 
//
// Replace the values of '<YourUniqueClientIdentifier>' and '<YourCustomEndpoint>'
// with a unique client identifier and custom host endpoint provided in AWS IoT.
// NOTE: client identifiers must be unique within your AWS account; if a client attempts 
// to connect with a client identifier which is already in use, the existing 
// connection will be terminated.
//
var device = awsIot.device({
   keyPath: 'private1thing.pem.key',
   certPath: 'certificate1thing.pem.crt',
   caPath: 'AmazonRootCA1.pem',
   clientId: 'MyConnect',
   host: 'am74972wsv881-ats.iot.us-east-2.amazonaws.com'
});
 
//
// Device is an instance returned by mqtt.Client(), see mqtt.js for full
// documentation.
//
device
  .on('connect', function() {
    console.log('connect');
    //device.subscribe('topic_1');
    device.subscribe('$aws/things/Sensor1/Humedad');
    device.subscribe('$aws/things/Sensor1/Temperatura');
  });
 
device
  .on('message', function(topic, payload) {
    console.log('message', topic, payload.toString());
  });