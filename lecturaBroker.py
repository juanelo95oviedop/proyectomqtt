# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 11:12:32 2020

@author: juano
"""
#importación de recursos
import time
import paho.mqtt.client as mqtt # import the client1
#configuración de broker
import serial
broker_address = "maqiatto.com"
broker_port = 1883
topic_H = "juanoviedoperdomo@hotmail.com/Humedad"
topic_T=  "juanoviedoperdomo@hotmail.com/Temperatura"
global temp, humed, recibido
limTemp=30
limHumed=50
resAnt=10
PuertoSerie = serial.Serial('COM3', 115200)
#definición de mensaje
def on_message(client, userdata, message):
    global recibido
    print("Mensaje recibido=", str(message.payload.decode("utf-8")))
    recibido=message.topic
    print("Topic=", message.topic)
    ##print("Nivel de calidad [0|1|2]=", message.qos)
    ##print("Flag de retención o nuevo?=", message.retain)
 
#Lectura de datos en durante 20 segundos, si se desea permanente se debe meter en un ciclo while(1)

client = mqtt.Client()
client.username_pw_set("juanoviedoperdomo@hotmail.com","123456")
client.on_message = on_message

client.connect(broker_address) 

while True:
    client.loop_start() # Inicio del bucle
    client.subscribe(topic_T)
    #suscripción al topico definido.
    time.sleep(1) # Paramos el hilo para recibir mensajes.
    client.loop_stop() # Fin del bucle
    temp=recibido
    client.loop_start() # Inicio del bucle
    client.subscribe(topic_H)
    #suscripción al topico definido.
    time.sleep(1) # Paramos el hilo para recibir mensajes.
    client.loop_stop() # Fin del bucle
    humed=recibido
    if  temp<limTemp and humed<limHumed:
        res=1
    elif temp>limTemp and humed<limHumed:
        res=2
    elif temp<limTemp and humed>limHumed:
        res=3
    elif temp>limTemp and humed>limHumed:
        res=5
    else:
        res=0
    if res!=resAnt:
        PuertoSerie.write(res)
    res=resAnt
    
        
    
    
    
    