# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 19:30:44 2020

@author: juano
"""
# importación de recursos a python
# recurso de asincronismo, tiempo, mqtt, puerto serial
import time
import paho.mqtt.client as mqtt # import the client1
import asyncio
import nest_asyncio
nest_asyncio.apply()
import serial
#definición de conexiones
# broker gratis, topicos, puerto
# puerto serial para lectura de datos.
broker_address = "maqiatto.com"
broker_port = 1883
topic_T = "juanoviedoperdomo@hotmail.com/Temperatura"
topic_H = "juanoviedoperdomo@hotmail.com/Humedad"
PuertoSerie = serial.Serial('COM3', 115200)

import paho.mqtt.client as paho

#función de publicación en boker
def on_publish(client, userdata, mid):
    print("mid: "+str(mid))

#definición de lectura y tratamiento datos
async def run():
    global Temp, Humed
    # Abrimos el puerto del arduino a 9600
    
    # Creamos un buble sin fin
    # leemos hasta que encontarmos el final de linea
    serie = PuertoSerie.readline()
    # Mostramos el valor leido y eliminamos el salto de linea del final
    print( "Valor PSoC: " , serie)
    Temp  = str(sArduino[2:4])
    Humed = str(sArduino[8:10])
    time.sleep(5)

# ciclo infinito que lee y envía datos al boker
while True:
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
    
    client = paho.Client()
    client.username_pw_set("juanoviedoperdomo@hotmail.com", "123456")
    client.on_publish = on_publish
    client.connect(broker_address)
    client.loop_start()
    (rc, mid) = client.publish(topic_T, str(Temp), qos=0)
    (rc, mid) = client.publish(topic_H, str(Humed), qos=0)
    print("información del estado de la temperatura y humedad enviada al Broker MQTT")
    
    