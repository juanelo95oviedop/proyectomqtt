/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include <stdio.h>

uint32 delay=5000;


void VentBajaVel(){
    PWM_WriteCompare(150);
}
void VentMediaVel(){
    PWM_WriteCompare(200);
}
void VentApa(){
    PWM_WriteCompare(0);
}
void VentAltaVel(){
    PWM_WriteCompare(254);
}
CY_ISR(byteReceive){
    char a=UART_GetByte();
    
    /*         temp     humedad     co2
    si a=1;      ok         ok      ok
    si a=2;      up         ok      ok
    si a=3;      ok         up      ok
    si a=4;      ok         ok      up
    si a=5;      up         up      ok
    si a=6;      ok         up      up
    si a=7;      up         ok      up 
    si a=8;      up         up      up
    */
    switch(a){
        case '1':
        LCD_Position(1,1);
        LCD_PrintString("ALL OK");
        LED1_Write(0);
        LED2_Write(0);
        LED3_Write(0);
        VentApa();
        break;
        case '2':
        LCD_Position(1,1);
        LCD_PrintString("Temp.: Alta");
        VentBajaVel();
        LED1_Write(1);
        LED2_Write(0);
        LED3_Write(0);
        break; 
        case '3':
        LCD_Position(1,1);
        LCD_PrintString("Humed: Alta");
        VentBajaVel();
        LED1_Write(0);
        LED2_Write(1);
        LED3_Write(0);
        break; 
        case '4':
        LCD_Position(1,1);
        LCD_PrintString("CO2 Alto");
        VentBajaVel();
        LED1_Write(0);
        LED2_Write(0);
        LED3_Write(1);
        break; 
        case '5':
        LCD_Position(1,1);
        LCD_PrintString("Temp.: Alta, Humed: Alta");
        VentMediaVel();
        LED1_Write(1);
        LED2_Write(1);
        LED3_Write(0);
        break; 
        case '6':
        LCD_Position(1,1);
        LCD_PrintString("Humed: Alta, CO2: Alto");
        VentMediaVel();
        LED1_Write(0);
        LED2_Write(1);
        LED3_Write(1);
        break; 
        case '7':
        LCD_Position(1,1);
        LCD_PrintString("Temp.: Alta, CO2: Alto");
        VentMediaVel();
        LED1_Write(1);
        LED2_Write(0);
        LED3_Write(1);
        break; 
        case '8':
        LCD_Position(1,1);
        LCD_PrintString("EMERGENCIA");
        VentAltaVel();
        LED1_Write(1);
        LED2_Write(1);
        LED3_Write(1);
        break;
        default:
        break; 
    }
    if(a<=4){
        delay=5000;
    }else if(a<9){
        delay=1000;
    }else{
        delay=100;
    }
}
int main(void)
{
    CyGlobalIntEnable; // Enable global interrupts. 
    
    PWM_Start();
    UART_Start();
    
    
    LCD_Start();
    LCD_ClearDisplay();
    LCD_Position(0,0);
    isr_StartEx(byteReceive);
    // Place your initialization/startup code here (e.g. MyInst_Start()) 
    LCD_PrintString("Resultados:");
    UART_PutString("hola termite");
    VentApa();
    for(;;)
    {
        
    
        
        // Place your application code here. 
    }
}

/* [] END OF FILE */
