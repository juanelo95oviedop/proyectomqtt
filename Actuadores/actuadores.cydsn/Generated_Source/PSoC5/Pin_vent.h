/*******************************************************************************
* File Name: Pin_vent.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_vent_H) /* Pins Pin_vent_H */
#define CY_PINS_Pin_vent_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pin_vent_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Pin_vent__PORT == 15 && ((Pin_vent__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Pin_vent_Write(uint8 value);
void    Pin_vent_SetDriveMode(uint8 mode);
uint8   Pin_vent_ReadDataReg(void);
uint8   Pin_vent_Read(void);
void    Pin_vent_SetInterruptMode(uint16 position, uint16 mode);
uint8   Pin_vent_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Pin_vent_SetDriveMode() function.
     *  @{
     */
        #define Pin_vent_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Pin_vent_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Pin_vent_DM_RES_UP          PIN_DM_RES_UP
        #define Pin_vent_DM_RES_DWN         PIN_DM_RES_DWN
        #define Pin_vent_DM_OD_LO           PIN_DM_OD_LO
        #define Pin_vent_DM_OD_HI           PIN_DM_OD_HI
        #define Pin_vent_DM_STRONG          PIN_DM_STRONG
        #define Pin_vent_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Pin_vent_MASK               Pin_vent__MASK
#define Pin_vent_SHIFT              Pin_vent__SHIFT
#define Pin_vent_WIDTH              1u

/* Interrupt constants */
#if defined(Pin_vent__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Pin_vent_SetInterruptMode() function.
     *  @{
     */
        #define Pin_vent_INTR_NONE      (uint16)(0x0000u)
        #define Pin_vent_INTR_RISING    (uint16)(0x0001u)
        #define Pin_vent_INTR_FALLING   (uint16)(0x0002u)
        #define Pin_vent_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Pin_vent_INTR_MASK      (0x01u) 
#endif /* (Pin_vent__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pin_vent_PS                     (* (reg8 *) Pin_vent__PS)
/* Data Register */
#define Pin_vent_DR                     (* (reg8 *) Pin_vent__DR)
/* Port Number */
#define Pin_vent_PRT_NUM                (* (reg8 *) Pin_vent__PRT) 
/* Connect to Analog Globals */                                                  
#define Pin_vent_AG                     (* (reg8 *) Pin_vent__AG)                       
/* Analog MUX bux enable */
#define Pin_vent_AMUX                   (* (reg8 *) Pin_vent__AMUX) 
/* Bidirectional Enable */                                                        
#define Pin_vent_BIE                    (* (reg8 *) Pin_vent__BIE)
/* Bit-mask for Aliased Register Access */
#define Pin_vent_BIT_MASK               (* (reg8 *) Pin_vent__BIT_MASK)
/* Bypass Enable */
#define Pin_vent_BYP                    (* (reg8 *) Pin_vent__BYP)
/* Port wide control signals */                                                   
#define Pin_vent_CTL                    (* (reg8 *) Pin_vent__CTL)
/* Drive Modes */
#define Pin_vent_DM0                    (* (reg8 *) Pin_vent__DM0) 
#define Pin_vent_DM1                    (* (reg8 *) Pin_vent__DM1)
#define Pin_vent_DM2                    (* (reg8 *) Pin_vent__DM2) 
/* Input Buffer Disable Override */
#define Pin_vent_INP_DIS                (* (reg8 *) Pin_vent__INP_DIS)
/* LCD Common or Segment Drive */
#define Pin_vent_LCD_COM_SEG            (* (reg8 *) Pin_vent__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pin_vent_LCD_EN                 (* (reg8 *) Pin_vent__LCD_EN)
/* Slew Rate Control */
#define Pin_vent_SLW                    (* (reg8 *) Pin_vent__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pin_vent_PRTDSI__CAPS_SEL       (* (reg8 *) Pin_vent__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pin_vent_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pin_vent__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pin_vent_PRTDSI__OE_SEL0        (* (reg8 *) Pin_vent__PRTDSI__OE_SEL0) 
#define Pin_vent_PRTDSI__OE_SEL1        (* (reg8 *) Pin_vent__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pin_vent_PRTDSI__OUT_SEL0       (* (reg8 *) Pin_vent__PRTDSI__OUT_SEL0) 
#define Pin_vent_PRTDSI__OUT_SEL1       (* (reg8 *) Pin_vent__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pin_vent_PRTDSI__SYNC_OUT       (* (reg8 *) Pin_vent__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Pin_vent__SIO_CFG)
    #define Pin_vent_SIO_HYST_EN        (* (reg8 *) Pin_vent__SIO_HYST_EN)
    #define Pin_vent_SIO_REG_HIFREQ     (* (reg8 *) Pin_vent__SIO_REG_HIFREQ)
    #define Pin_vent_SIO_CFG            (* (reg8 *) Pin_vent__SIO_CFG)
    #define Pin_vent_SIO_DIFF           (* (reg8 *) Pin_vent__SIO_DIFF)
#endif /* (Pin_vent__SIO_CFG) */

/* Interrupt Registers */
#if defined(Pin_vent__INTSTAT)
    #define Pin_vent_INTSTAT            (* (reg8 *) Pin_vent__INTSTAT)
    #define Pin_vent_SNAP               (* (reg8 *) Pin_vent__SNAP)
    
	#define Pin_vent_0_INTTYPE_REG 		(* (reg8 *) Pin_vent__0__INTTYPE)
#endif /* (Pin_vent__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Pin_vent_H */


/* [] END OF FILE */
